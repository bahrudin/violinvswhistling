#process 

# 1 patak 
# 2 7th sym
# 3 bosno moja
# 4 uskudara
# 5 ode to joy
# 6 mad world
# 7 soto 
# 8 cvjetak 
# 9 lane moje
#10 rastanak

import numpy as np
import matplotlib.pyplot as plt
import time
from datetime import datetime
import os

curDoc = os.listdir()
wnames = [ele for ele in curDoc if ele.endswith(".npy")]

freqs = []
powers= []
times = []
names = []

for name in wnames:
    L = 305 - len(np.load(name)[1])
    a = np.concatenate((np.load(name)[1],np.load(name)[1],np.load(name)[1]))
    freqs.append(a[:240])
    
    b = np.concatenate((np.load(name)[2],np.array([0,]*L)))
    powers.append(b)
    
    c = np.concatenate((np.load(name)[0],np.array([0,]*L)))
    times.append(c)
    
    names.append(name[:-5])
    
freqs = np.array(freqs)
freqs[freqs<350] = freqs.mean()
freqs[freqs>2500] = freqs.mean()

powers= np.array(powers)
times = np.array(times)
#%%
ratios = []
for vv in range(10):
    v11 = freqs[30+vv]
    #print(names[30+vv])
    
    kk=3*vv+1
    z11 = freqs[kk]
    #print(names[kk])
    rat = np.mean(z11/v11)
    ratios.append(rat)
    
#%%
vv = 0
v11 = freqs[30+vv]
print(names[30+vv])


kk=3*vv+2
z11 = freqs[kk]/ratios[vv]
print(names[kk])

plt.figure(figsize=(14,5))
plt.stem(v11, markerfmt='bo', label='Violin',use_line_collection=True)
plt.stem(z11, markerfmt='go', label='Whistle',use_line_collection=True)
plt.legend()

#%%

from sklearn.neighbors import KNeighborsClassifier
neigh = KNeighborsClassifier(n_neighbors=3)
neigh.fit(freqs[:30], names[:30])

for pre in range(10):
    sampl = freqs[30+pre]*ratios[pre]
    print(neigh.predict([sampl]),'--',names[30+pre])


#%%
from sklearn.linear_model import SGDClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline

X = freqs[:30]
X[X>1200]=X.mean()
Y = names[:30]

clf = make_pipeline(StandardScaler(),
                    SGDClassifier(max_iter=10000, tol=1e-2,loss='squared_hinge'))
clf.fit(X, Y)

for pre in range(10):
    sampl = freqs[30+pre]*ratios[pre]
    print(clf.predict([sampl]),'--',names[30+pre])
       
    
#%%
    
num = 6
print(names[30+num])
def stepwise(n=10):
    from sklearn.neighbors import KNeighborsClassifier
    
    freqs = []
    powers= []
    times = []
    names = []
    
    for name in wnames:
        L = 305 - len(np.load(name)[1])
        a = np.concatenate((np.load(name)[1],np.array([0,]*L)))
        freqs.append(a[:n])
        
        b = np.concatenate((np.load(name)[2],np.array([0,]*L)))
        powers.append(b[:n])
        
        c = np.concatenate((np.load(name)[0],np.array([0,]*L)))
        times.append(c[:n])
        
        names.append(name[:-5])
        
    freqs = np.array(freqs)
    powers= np.array(powers)
    times = np.array(times)
    
    neigh = KNeighborsClassifier(n_neighbors=1)
    neigh.fit(freqs[:30], names[:30])
    
    print(neigh.predict( [freqs[30+num][:n]*ratios[num]] ))
    
for n in range(10,300,20):
    stepwise(n)

    


#%
