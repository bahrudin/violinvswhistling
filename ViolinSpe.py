############### Import Libraries ###############
import pyaudio
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.mlab import window_hanning,specgram
import matplotlib.animation as animation
from matplotlib.colors import LogNorm

############### Constants ###############
rate = 44100 #sample rate
FORMAT = pyaudio.paInt16 #conversion format for PyAudio stream
CHANNELS = 1 #microphone audio channels
CHUNK_SIZE = 8192 #number of samples to take per read
SAMPLE_LENGTH = int(CHUNK_SIZE*1000/rate) #length of each sample in ms
SAMPLES_PER_FRAME = 8
nfft = 1024#256#1024 #NFFT value for spectrogram
overlap = 1000#512 #overlap value for spectrogram

def open_mic():
    pa = pyaudio.PyAudio()
    stream = pa.open(format = FORMAT,
                     channels = CHANNELS,
                     rate = rate,
                     input = True,
                     frames_per_buffer = CHUNK_SIZE)
    return stream,pa


def get_data(stream,pa):
    input_data = stream.read(CHUNK_SIZE)
    data = np.fromstring(input_data,np.int16)
    return data


def get_sample(stream,pa):
    input_data = stream.read(CHUNK_SIZE)
    data = np.fromstring(input_data,np.int16)
    return data


def get_specgram(signal,rate):
    down,up = 5,100
    arr2D,freqs,bins = specgram(signal,window=window_hanning,
                                Fs = rate,NFFT=nfft,noverlap=overlap)
    return arr2D[down:up,:],freqs[down:up],bins

def update_fig(n):
    data = get_sample(stream,pa)
    arr2D,freqs,bins = get_specgram(data,rate)
    im_data = im.get_array()
    if n < SAMPLES_PER_FRAME:
        im_data = np.hstack((im_data,arr2D))
        im.set_array(im_data)
    else:
        keep_block = arr2D.shape[1]*(SAMPLES_PER_FRAME - 1)
        im_data = np.delete(im_data,np.s_[:-keep_block],1)
        im_data = np.hstack((im_data,arr2D))
        im.set_array(im_data)
    return im,


fig = plt.figure()
stream,pa = open_mic()
data = get_sample(stream,pa)
arr2D,freqs,bins = get_specgram(data,rate)


extent = (bins[0],bins[-1]*SAMPLES_PER_FRAME,freqs[-1],freqs[0])
im = plt.imshow(arr2D,aspect='auto',extent = extent,interpolation="none",
                cmap = 'jet',norm = LogNorm(vmin=.01,vmax=1))

plt.xlabel('Time (s)')
plt.ylabel('Frequency (Hz)')
plt.title('Real Time Spectogram')
plt.gca().invert_yaxis()

############### Animate ###############
anim = animation.FuncAnimation(fig,update_fig,blit = 1,
                            interval=CHUNK_SIZE/1000)

