import pyaudio
import numpy as np
import matplotlib.pyplot as plt
import time
from datetime import datetime

mult = 1


def plot_data(in_data):
    audio_data = np.fromstring(in_data, np.int16)
    dfft = 10.*np.log10(np.abs(np.fft.rfft(audio_data)))
        
    dfft = dfft[dwn:up]
    li.set_xdata(np.arange(len(audio_data)))
    li.set_ydata(audio_data)
    
    li2.set_xdata(np.arange(dwn,up)*ratio)
    DFT = dfft - np.average(dfft)
    li2.set_ydata(DFT)
    
    myM.append(DFT)
    
    frequency_detected = round((int(np.argmax(dfft) + dwn)*ratio)) 
    res = 'Frequency: ' + str(frequency_detected)+ ' Hz'
    
    ax[0].set_title(res)
    
    plt.pause(0.01)
    if keep_going:
        return True
    else:
        return False

def calc_time(now1):
    now2 = datetime.now()
    dt = now2-now1 
    return np.round(dt.microseconds/1000000,3)+dt.seconds


i=0
f,ax = plt.subplots(2)

x = np.arange(10000)
y = np.random.randn(10000)

li, = ax[0].plot(x, y)
ax[0].set_xlim(0,1000)
ax[0].set_ylim(-5000,5000)
ax[0].set_title("Raw Audio Signal")

# 800Hz to 2500Hz

dwn,up = 5*mult, 110*mult #max 513
FORMAT = pyaudio.paInt16 # We use 16bit format per sample
CHANNELS = 1
RATE = 44100
CHUNK = 1024 *6 # 1024bytes of data red from a buffer
RECORD_SECONDS = 0.1
keep_going = True
ratio = round(RATE/CHUNK,3)

li2, = ax[1].plot(x, y)
ax[1].set_xlim(dwn*ratio,up*ratio)
ax[1].set_ylim(-50,50)
ax[1].set_title("Fast Fourier Transform")


plt.pause(0.01)
plt.tight_layout()

audio = pyaudio.PyAudio()
stream = audio.open(format=FORMAT,
                    channels=CHANNELS,
                    rate=RATE,
                    input=True)#,
                    #frames_per_buffer=CHUNK)

myM = []

print("Starting in 2")
time.sleep(1)
print("Starting in 1")
time.sleep(1)
print("Starting NOW")

########
now1 = datetime.now()
stream.start_stream()
while keep_going:
    try:
        plot_data(stream.read(CHUNK))
    except KeyboardInterrupt:
        keep_going=False
        now2 = datetime.now()
        dur = calc_time(now1)
        print('exe time = ',dur)
    except:
        now2 = datetime.now()
        dur = calc_time(now1)
        print('exe time = ',dur)
        break

stream.stop_stream()
stream.close()
audio.terminate()

#%%
myM = np.array(myM)
extent = [0,dur, dwn*ratio,up*ratio]
aspect =0.5*dur/((up-dwn)*ratio)
plt.imshow(myM.T,extent=extent,aspect=aspect,origin='lover')

#%%
from scipy.signal import find_peaks

freqs = []
FREQS = []
power = []

for ele in myM:
    frequency_detected = round((int(np.argmax(ele) + dwn)*ratio)) 
    freqs.append(frequency_detected)
    
    peaks = find_peaks(ele,distance =40)
    FREQuency_detected = round((int(peaks[0][0] + dwn)*ratio))
    FREQS.append(FREQuency_detected)
    
    pwr = np.max(ele)
    power.append(pwr)

#%%
begining,ending = 0,-1
power = (np.array(power) - np.mean(power)) / np.std(power)
freqs = np.array(freqs)

#power analysis
for i in range(1,len(power)//4):
    if np.mean(power[i:i+3]<-1.0) and np.mean(power[i+4:i+7]>0):
        begining = i+3
        print(begining)
        
    if np.mean(power[-i-3:-i]<-0.5) and np.mean(power[-i-10:-i-4]>0.0):
        ending = -i-3
        print(ending)

tme = np.arange(len(power))*dur/len(power)

plt.figure(figsize=(10,8))
plt.title("Frequency vs. time")
plt.subplot("211")    
plt.plot(tme[begining:ending],
         freqs[begining:ending]/2,'--')
plt.plot(tme[begining:ending],
         FREQS[begining:ending],'--',alpha=0.3)
plt.xlabel('Time [s]')
plt.legend(['Second Harmonic $/$ 2','First Harmonic'])

plt.subplot("212")
plt.plot(tme[begining:ending],
         power[begining:ending])
plt.xlabel('Time [s]')

# 1 patak 
# 2 7th sym
# 3 bosno moja
# 4 uskudara
# 5 ode to joy
# 6 mad world
# 7 soto 
# 8 cvjetak 
# 9 lane moje
#10 rastanak

toSave = [tme[begining:ending],
         freqs[begining:ending],
         power[begining:ending]]

np.save('v101.npy',toSave)

#%%
